---

title:  GitLab Laptop Delivery Metrics
---





## GitLab Laptop Delivery Metrics

### Purpose

We're designing this page to publicly and transparently show our metrics around laptop shipping, delivery, and procurement. This page is in its early stage and we will iterate and add more content as we continue to advance our metrics.

### Table Breakdown

The **Regions** section is detailing what region and what metric we are tracking, within we are reporting total laptops delivered per region and percentage of on time delivery for the related month. The acronym ROW stands for **Rest of the World**, these are region that we hire in but do not have a registered vendor or shipping entity. Please note these numbers do not reflect new hires or team members that opt to self-procure their own laptop.



| Regions                         | June | July | August | September |October |November |
| -------------                   |:-----|:-----|:-------|:----------|:-------|:--------|
| EMEA Laptops delivered          |4     |11    |9       |8          |9       |9
| EMEA % on time                  |100%  |100%  |100%    |87.5%      |100%    |100%
| North America laptops delivered |15    |22    |22      |22         |27      |23
| North America % on time         |93.4% |92%   |95.5%   |100%       |92%     |91%
| APAC Laptops delivered          |6     |6     |3       |3          |2       |3
| APAC % on time                  |66.70%|100%  |67%     |100%       |100%    |100%
| LATAM Laptops delivered         |0     |0     |0       |0          |0       |0
| LATAM % on time                 |N/A   |N/A   |N/A     |N/A        |N/A     |N/A
| ROW Laptops delivered           |0     |0     |0       |0          |0       |0
| ROW % on time                   |N/A   |N/A   |N/A     |N/A        |N/A     |N/A
| Total laptops delivered         |25    |39    |34      |33         |39      |37
| % Laptops delivered on time     |88%   |95%   |94%     |97.2%      |94.9%   |94.6%

